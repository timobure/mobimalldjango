from rest_framework import serializers
from .models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    model = CustomUser
    fields = ('first_name', 'last_name', 'email', 'username', 'password')