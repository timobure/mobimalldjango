from django.urls import path
from . import views


app_name = 'sales'

urlpatterns = [
    path('', views.SaleList.as_view(), name='sales'),
    path('create/', views.SaleCreate.as_view(), name='sale_create'),
    path('update/<int:pk>/', views.SaleUpdate.as_view(), name='sale_update'),
    path('<int:pk>/', views.SaleDetail.as_view(), name='sale'),

    path('payments/', views.PaymentList.as_view(), name='payments'),
    path('payments/create/', views.PaymentCreate.as_view(), name='payments_create'),
    path('payments/update/<int:pk>/', views.PaymentUpdate.as_view(), name='payments_update'),
    path('payments/<int:pk>/', views.PaymentDetail.as_view(), name='payment'),

    path('receipts/', views.ReceiptList.as_view(), name="receipts"),
    path('receipts/<int:pk>/', views.ReceiptList.as_view(), name="receipt"),
    path('summary/', views.SalesSummary.as_view(), name="summary"),
]

