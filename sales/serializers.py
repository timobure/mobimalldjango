from rest_framework import serializers
from .models import Sale, PaymentMethod, Receipt


class SaleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = ('id', 'product', 'price', 'sell_time', 'payment_method', 'receipt')
        depth = 1

class SaleSingleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sale
        fields = ('id', 'product', 'price', 'sell_time', 'payment_method', 'receipt')

    
class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethod
        fields = ('id', 'name', 'prefix')


class ReceiptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receipt
        fields = ('number',)
