from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response
from products.models import Product, Price
from .models import Sale, PaymentMethod, Receipt
from .serializers import SaleSerializer, SaleSingleSerializer, PaymentSerializer, ReceiptSerializer


class SaleList(generics.ListCreateAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


class SaleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSerializer


class SaleCreate(generics.CreateAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSingleSerializer


class SaleUpdate(generics.UpdateAPIView):
    queryset = Sale.objects.all()
    serializer_class = SaleSingleSerializer

# PaymentMethods
class PaymentList(generics.ListCreateAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentSerializer


class PaymentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentSerializer


class PaymentCreate(generics.CreateAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentSerializer


class PaymentUpdate(generics.UpdateAPIView):
    queryset = PaymentMethod.objects.all()
    serializer_class = PaymentSerializer

# Receipts
class ReceiptList(generics.ListCreateAPIView):
    queryset = Receipt.objects.all()
    serializer_class = ReceiptSerializer


class ReceiptEdit(generics.RetrieveUpdateDestroyAPIView):
    queryset = Receipt.objects.all()
    serializer_class = ReceiptSerializer


class SalesSummary(APIView):

    def get(self, request):
        product_count = self.get_products()
        sale_data = self.get_profit()
        return Response({'product_count':product_count, 'sale_data': sale_data})

    def get_products(self):
        product_list: list = []
        products = Product.objects.all().order_by('name')
        product: Product
        for product in products:
            product_list.append({'name': product.name, 'quantity': product.sales.all().count()})
        product_list.sort(key=lambda x: x['quantity'], reverse=True)
        return product_list

    def get_profit(self)->dict:
        sale: Sale
        total_buy: float = 0.00
        total_sell: float = 0.00
        sales = Sale.objects.all()
        for sale in sales:
            price: Price = self.get_prices(sale)
            total_buy += round(float(price.buy_price), 2)
            total_sell += round(float(price.sell_price), 2)
        profit: float = total_sell - total_buy
        return {'Bought': total_buy, 'Sold': total_sell, 'Profit': profit}

    def get_prices(self, sale: Sale)->Price:
        product: Product = sale.product
        # Get price based on time sold
        price: Price = Price.objects.filter(product=product, load_date__lt=sale.sell_time).order_by('-load_date')[0]
        return price
        


