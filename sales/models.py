from django.db import models, IntegrityError
from django.db.models.signals import post_save
from django.dispatch import receiver
from products.models import Product, ProductQuantity


class PaymentMethod(models.Model):
    name = models.CharField(max_length=25, unique=True)
    prefix = models.CharField(max_length=5)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.name: str = self.name.title()
        self.prefix: str = self.prefix.upper()
        super().save(**kwargs)


class Receipt(models.Model):
    number = models.AutoField(primary_key=True)


class Sale(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="sales")
    sell_time = models.DateTimeField(auto_now=True)
    price = models.DecimalField(max_digits=7, decimal_places=2)
    payment_method = models.ForeignKey(PaymentMethod, on_delete=models.CASCADE)
    receipt = models.ForeignKey(Receipt, on_delete=models.SET_NULL, null=True)

# Reduce product quantity with each sale
@receiver(post_save, sender=Sale)
def adjust_quantities(sender, instance:Sale, created:bool, **kwargs):
    qtt_obj: ProductQuantity = ProductQuantity.objects.get(pk=instance.product.pk)
    qtt_obj.quantity -= 1
    qtt_obj.save()


    
