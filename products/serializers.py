from rest_framework import serializers
from .models import Category, Product, Price, ProductQuantity


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name', 'details')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'code', 'category')
        depth = 1

class ProductSingleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'code', 'category')


class ProductQttSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductQuantity
        fields = ('id', 'product', 'quantity')
        depth = 1


class ProductQttSingleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductQuantity
        fields = ('id', 'product', 'quantity')


class PriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ('id', 'product', 'buy_price', 'sell_price', 'load_date')
        depth = 1


class PriceSingleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Price
        fields = ('id', 'product', 'buy_price', 'sell_price', 'load_date')

