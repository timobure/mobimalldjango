from django.urls import path
from . import views


app_name = 'products'

urlpatterns = [
    path('', views.ProductList.as_view(), name='products'),
    path('create/', views.ProductList.as_view(), name='product_create'),
    path('<int:pk>/', views.ProductDetail.as_view(), name='product'),

    path('categories/', views.CategoryList.as_view(), name='categories'),
    path('categories/create/', views.CategoryCreate.as_view(), name='category_create'),
    path('category/<int:pk>/', views.CategoryDetail.as_view(), name='category'),

    path('prices/', views.PriceList.as_view(), name='prices'),
    path('price/create/', views.PriceList.as_view(), name='price_create'),
    path('price/<int:pk>/', views.PriceDetail.as_view(), name='prices'),

    path('quantities/', views.ProductQttList.as_view(), name='quantities'),
    path('quantity/<int:pk>/', views.ProductQttDetail.as_view(), name='quantity'),
]
