from rest_framework import generics, mixins
from rest_framework.views import APIView
from .models import Category, Product, Price, ProductQuantity
from .serializers import CategorySerializer, ProductSerializer, ProductSingleSerializer, \
    PriceSerializer, PriceSingleSerializer, ProductQttSerializer, ProductQttSingleSerializer


class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryCreate(generics.CreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryUpdate(generics.UpdateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


# Products
class ProductList(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSingleSerializer


class ProductCreate(generics.CreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSingleSerializer


class ProductUpdate(generics.UpdateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSingleSerializer

# Price
class ProductPrice(generics.RetrieveAPIView):
    # queryset = Price.objects.filter(product_id=1).order_by('-load_date')
    serializer_class = PriceSerializer

    def get_object(self):
        return Price.objects.filter(product_id=self.kwargs['pk']).order_by('-load_date')[0]

class PriceList(generics.ListCreateAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer


class PriceDetail(generics.RetrieveDestroyAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSerializer


class PriceCreate(generics.CreateAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSingleSerializer


class PriceUpdate(generics.UpdateAPIView):
    queryset = Price.objects.all()
    serializer_class = PriceSingleSerializer


class ProductQttList(generics.ListCreateAPIView):
    queryset = ProductQuantity.objects.all()
    serializer_class = ProductQttSerializer


class ProductQttDetail(generics.RetrieveDestroyAPIView):
    queryset = ProductQuantity.objects.all()
    serializer_class = ProductQttSerializer


class ProductQttUpdate(generics.UpdateAPIView):
    queryset = ProductQuantity.objects.all()
    serializer_class = ProductQttSingleSerializer



