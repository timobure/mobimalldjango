import json
from django.test import TestCase
from django.urls import reverse
from .models import Category, Product, ProductQuantity, Price
from .serializers import CategorySerializer

class CategoryTest(TestCase):
    def setUp(self):
        # Create categories
        Category.objects.create(name="Food Stuff", details="Processed foods"),
        Category.objects.create(name="Drinks", details="Soft non-alcoholic drinks")

        self.valid_category = {
            "name": "Vegetable",
            "details": "All farm produce"
        }
        self.valid_category2 = {
            "name": "Grains",
            "details": "Grains and cereals"
        }
        self.invalid_category = {
            "name": "",
            "details": "Not defined"
        }
        

    def test_list(self):
        # All Categories are loading
        response = self.client.get(reverse('products:categories'))
        assert response.status_code == 200

    def test_retrieve(self):
        # Single category is fetching
        response = self.client.get(reverse('products:category', kwargs={'pk':1}))
        assert response.status_code == 200
        # Wrong category is not fetching
        response = self.client.get(reverse('products:category', kwargs={'pk':40}))
        assert response.status_code == 404

    def test_post(self):
        # Correct post
        response = self.client.post(
            reverse('products:category_create'),
            data=json.dumps(self.valid_category),
            content_type='application/json'
        )
        assert response.status_code == 201
        # InCorrect post
        response = self.client.post(
            reverse('products:category_create'),
            data=json.dumps(self.invalid_category),
            content_type='application/json'
        )
        assert response.status_code == 400

    def test_put(self):
        # Correct put
        response = self.client.put(
            reverse('products:category_update', kwargs={'pk':1}),
            data=json.dumps(self.valid_category2),
            content_type='application/json'
        )
        assert response.status_code == 200
        # InCorrect put
        response = self.client.post(
            reverse('products:category_update', kwargs={'pk':1}),
            data=json.dumps(self.invalid_category),
            content_type='application/json'
        )
        assert response.status_code == 405

    def test_delete(self):
        # Valid delete
        response = self.client.delete(reverse('products:category', kwargs={'pk':1}))
        assert response.status_code == 204
        # Invalid delete
        response = self.client.delete(reverse('products:category', kwargs={'pk':40}))
        assert response.status_code == 404




