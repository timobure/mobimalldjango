from django.urls import path
from . import views


app_name = 'products'

urlpatterns = [
    path('', views.ProductList.as_view(), name='products'),
    path('create/', views.ProductCreate.as_view(), name='product_create'),
    path('update/<int:pk>/', views.ProductUpdate.as_view(), name='product_update'),
    path('<int:pk>/', views.ProductDetail.as_view(), name='product'),

    path('categories/', views.CategoryList.as_view(), name='categories'),
    path('categories/create/', views.CategoryCreate.as_view(), name='category_create'),
    path('categories/update/<int:pk>/', views.CategoryUpdate.as_view(), name='category_update'),
    path('categories/<int:pk>/', views.CategoryDetail.as_view(), name='category'),

    path('prices/', views.PriceList.as_view(), name='prices'),
    path('prices/create/', views.PriceCreate.as_view(), name='price_create'),
    path('prices/update/<int:pk>/', views.PriceUpdate.as_view(), name='price_update'),
    path('prices/<int:pk>/', views.PriceDetail.as_view(), name='prices'),
    path('price/<int:pk>/', views.ProductPrice.as_view(), name='price'),

    path('quantities/', views.ProductQttList.as_view(), name='quantities'),
    path('quantities/update/<int:pk>/', views.ProductQttUpdate.as_view(), name='quantity_update'),
    path('quantities/<int:pk>/', views.ProductQttDetail.as_view(), name='quantity'),
]
