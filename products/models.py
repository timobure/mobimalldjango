from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Category(models.Model):
    """ Category of each Product """
    name = models.CharField(max_length=25, unique=True)
    details = models.TextField()

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        self.name: str = self.name.title()
        self.details: str = self.details.title()
        super().save(**kwargs)
        

class Product(models.Model):
    """ The Product """
    name = models.CharField(max_length=25, unique=True)
    code = models.CharField(max_length=25, unique=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    # objects = ProductManager()

    def __str__(self)-> str:
        return self.name

    def save(self, **kwargs):
        self.name: str = self.name.title()
        self.code: str = self.code.upper()
        super().save(**kwargs)

    def price(self)-> int:
        """ The last uploaded price of a Product is its current selling price """
        price: Price = Price.objects.filter(product=self).order_by('load_date')
        return price.sell_price

    def available(self)->bool:
        query: ProductQuantity = ProductQuantity.objects.get(product=self)
        if query.quantity > 0:
            return True
        return False


class ProductQuantity(models.Model):
    """
    Stores the product quantity
    Quantity is changed on each sale action
    """
    product = models.OneToOneField(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)

# A signal to create a new instance of product quantity whenever a new product is saved
@receiver(post_save, sender=Product)
def create_product_quantity(sender, instance, created, **kwargs):
    if created:
        ProductQuantity.objects.create(product=instance, quantity=0)


class Price(models.Model):
    """
    Sets Pricing for each Product
    The are no unique product
    The last product price sets its current price
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    buy_price = models.DecimalField(max_digits=7, decimal_places=2)
    sell_price = models.DecimalField(max_digits=7, decimal_places=2)
    load_date = models.DateTimeField(auto_now_add=True)

    # objects = PriceManager()

    def __str__(self)-> str:
        return self.product

    

