<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Mobi Mall Django</h3>

  <p align="center">
    A Django API backend for a simple sales application
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Contact](#contact)


## About The Project

This is the backend of a simple sales application with an android and web admin dashboard fron-tends.
It exposes the API for consumption by the latter.



### Built With

* []()Python
* []()Django
* []()Django Rest Framework



## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* Python 3.7+

### Installation
 
1. Clone the repo
```sh
git clone https://timobure@bitbucket.org/timobure/mobimalldjango.git
```
2. Install pip packages
```sh
pip install -r requirements.txt
```
Fire up the django server and visit the urls as in the project urls.py files.

## License

Distributed under the MIT License. See `LICENSE` for more information.


## Contact

Timon Obure - timobure@gmail.com

Project Link: https://timobure@bitbucket.org/timobure/mobimalldjango.git